package br.com.sergiosilvajr.fametroapp.login;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;

import br.com.sergiosilvajr.fametroapp.R;

/**
 * Created by sergi on 05/05/2017.
 */

public class ForgotPasswordActivity extends AppCompatActivity {
    private FirebaseAuth firebaseAuth;
    private EditText emailToSend;
    private Button sendEmail;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        firebaseAuth = FirebaseAuth.getInstance();
        setContentView(R.layout.activity_forgot_password);
        emailToSend = (EditText) findViewById(R.id.send_email);
        sendEmail = (Button) findViewById(R.id.forgot_password_button);

        sendEmail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String email = emailToSend.getText().toString();
                firebaseAuth.sendPasswordResetEmail(email)
                        .addOnCompleteListener(new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {
                                if (task.isSuccessful()) {
                                    Toast.makeText(ForgotPasswordActivity.this, R.string.email_sent, Toast.LENGTH_SHORT).show();
                                } else {
                                    Toast.makeText(ForgotPasswordActivity.this, R.string.problem_sending_email, Toast.LENGTH_SHORT).show();
                                }
                            }
                        });
            }
        });
    }
}
