package br.com.sergiosilvajr.fametroapp.login;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

import br.com.sergiosilvajr.fametroapp.R;

/**
 * Created by sergi on 05/05/2017.
 */

public class SignupActivity extends AppCompatActivity {
    private FirebaseAuth firebaseAuth;
    private EditText loginEditText;
    private EditText passwordEditText;
    private Button signupButton;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        firebaseAuth = FirebaseAuth.getInstance();

        setContentView(R.layout.activity_signup);

        loginEditText = (EditText) findViewById(R.id.login_input);
        passwordEditText = (EditText) findViewById(R.id.password_input);
        signupButton = (Button) findViewById(R.id.signup_button);
        signupButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String login = loginEditText.getText().toString();
                String password = passwordEditText.getText().toString();
                firebaseAuth.createUserWithEmailAndPassword(login, password)
                        .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                            @Override
                            public void onComplete(@NonNull Task<AuthResult> task) {
                                if (task.isSuccessful()) {
                                    Toast.makeText(SignupActivity.this, R.string.user_created, Toast.LENGTH_SHORT).show();
                                } else {
                                    Toast.makeText(SignupActivity.this, R.string.problem_creating_user, Toast.LENGTH_SHORT).show();
                                }
                            }
                        });
            }
        });
    }
}
