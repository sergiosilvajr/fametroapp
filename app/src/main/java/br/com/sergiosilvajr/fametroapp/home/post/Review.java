package br.com.sergiosilvajr.fametroapp.home.post;

/**
 * Created by sergi on 05/05/2017.
 */

public class Review {
    public static final String modelRef = "reviews";
    public static final String messageRef = "message";
    public static final String userRef = "uid";
    public static final String scoreRef = "score";
    public static final String dateRef = "time";

    private String message;
    private String uID;
    private String timeStamp;
    private String score;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getuID() {
        return uID;
    }

    public void setuID(String uID) {
        this.uID = uID;
    }

    public String getScore() {
        return score;
    }

    public void setScore(String score) {
        this.score = score;
    }

    public String getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(String timeStamp) {
        this.timeStamp = timeStamp;
    }
}
