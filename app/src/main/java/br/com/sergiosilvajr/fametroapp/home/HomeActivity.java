package br.com.sergiosilvajr.fametroapp.home;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import br.com.sergiosilvajr.fametroapp.R;
import br.com.sergiosilvajr.fametroapp.home.post.CommentAdapter;
import br.com.sergiosilvajr.fametroapp.home.post.PostReviewActivity;
import br.com.sergiosilvajr.fametroapp.home.post.Review;

/**
 * Created by sergi on 05/05/2017.
 */

public class HomeActivity extends AppCompatActivity {
    private Button postReview;
    private RecyclerView recyclerView;
    private CommentAdapter commentAdapter;

    private FirebaseDatabase firebaseDatabase;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        firebaseDatabase = FirebaseDatabase.getInstance();
        recyclerView = (RecyclerView) findViewById(R.id.comment_list);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        commentAdapter = new CommentAdapter();
        recyclerView.setAdapter(commentAdapter);

        postReview = (Button) findViewById(R.id.add_new_comment);
        postReview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(HomeActivity.this, PostReviewActivity.class));
            }
        });
        firebaseRetrieveReviews();
    }

    private void firebaseRetrieveReviews() {
        firebaseDatabase.getReference()
                .child(Review.modelRef)
                .orderByChild(Review.dateRef)
                .addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        List<Review> reviews = new ArrayList<>();
                        for (DataSnapshot childSnapshot : dataSnapshot.getChildren()) {
                            Map map = (Map) childSnapshot.getValue();
                            Review newReview = new Review();
                            newReview.setScore((String) map.get(Review.scoreRef));
                            newReview.setuID((String) map.get(Review.userRef));

                            newReview.setTimeStamp(Long.toString((Long) map.get(Review.dateRef)));
                            newReview.setMessage((String) map.get(Review.messageRef));
                            reviews.add(newReview);
                        }
                        Collections.reverse(reviews);
                        commentAdapter.addNewReviewList(reviews);
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
    }
}
