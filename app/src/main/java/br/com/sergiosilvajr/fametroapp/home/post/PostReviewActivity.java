package br.com.sergiosilvajr.fametroapp.home.post;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ServerValue;
import com.google.firebase.database.ValueEventListener;

import java.util.HashMap;
import java.util.Map;

import br.com.sergiosilvajr.fametroapp.R;

/**
 * Created by sergi on 05/05/2017.
 */

public class PostReviewActivity extends AppCompatActivity {
    private FirebaseAuth firebaseAuth;
    private FirebaseDatabase firebaseDatabase;

    private EditText messageText;
    private Spinner spinner;
    private Button newReviewButton;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post_review);
        firebaseAuth = FirebaseAuth.getInstance();
        firebaseDatabase= FirebaseDatabase.getInstance();

        messageText = (EditText) findViewById(R.id.review_message);
        spinner = (Spinner) findViewById(R.id.score_spinner);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.scores, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);

        newReviewButton = (Button) findViewById(R.id.post_review);
        newReviewButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String uId = firebaseAuth.getCurrentUser().getUid();

                Review review = new Review();
                review.setuID(uId);
                review.setScore((String) spinner.getSelectedItem());
                review.setMessage(messageText.getText().toString());

                firebaseDatabase.getReference()
                        .child(Review.modelRef)
                        .push()
                        .updateChildren(getMapFromReview(review))
                        .addOnCompleteListener(new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {
                                if (task.isSuccessful()) {

                                } else {

                                }
                            }
                        });
            }
        });


    }

    private Map<String, Object> getMapFromReview(Review review) {
        Map<String, Object> map = new HashMap<>();
        map.put(Review.messageRef, review.getMessage());
        map.put(Review.userRef, firebaseAuth.getCurrentUser().getUid());
        map.put(Review.scoreRef, review.getScore());
        map.put(Review.dateRef, ServerValue.TIMESTAMP);
        return map;
    }
}
