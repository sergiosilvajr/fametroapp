package br.com.sergiosilvajr.fametroapp.home.post;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import br.com.sergiosilvajr.fametroapp.R;

/**
 * Created by sergi on 05/05/2017.
 */

public class CommentAdapter extends RecyclerView.Adapter<CommentAdapter.CommentViewHolder> {
    private List<Review> reviews = new ArrayList<>();

    @Override
    public CommentViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View root = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_review, parent, false);
        CommentViewHolder holder = new CommentViewHolder(root);

        return holder;
    }

    @Override
    public void onBindViewHolder(CommentViewHolder holder, int position) {
        Review review = reviews.get(position);
        holder.message.setText(review.getMessage());
        holder.score.setText(review.getScore());
    }

    public void addNewReviewList(List<Review> reviews) {
        this.reviews = reviews;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        if (reviews != null) {
            return reviews.size();
        } else {
            return 0;
        }
    }

    static class CommentViewHolder extends RecyclerView.ViewHolder {
        TextView message;
        TextView score;

        public CommentViewHolder(View itemView) {
            super(itemView);
            message = (TextView) itemView.findViewById(R.id.review_item_message);
            score = (TextView) itemView.findViewById(R.id.review_item_score);
        }
    }
}
